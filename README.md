.po -> byte array
  std::embed
  phd::embed
  cmake + xxd + embed-wrapper
	  constexpr std::span<const std::byte> data = l18n::embed("sv_SE.po");

byte array -> parsed structure
  spirit-po
  own parser

parsed structure -> catalogue
  own thing

catalogue -> .pot

catalogue -> application
  constexpr gettext api

add_executable(example)

target_sources(example PRIVATE main.cpp)

target_link_libraries(example PUBLIC
  l18n
)

target_include_directories(example PRIVATE ${CMAKE_SOURCE_DIR}/src)

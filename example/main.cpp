#include <iostream>
#include <catalogue.hpp>

//class locale
//{
//
//  template <fixstr::fixed_string key>
//  constexpr auto gettext()
//  {
//    return my_library.gettext<key>();
//  }
//};


int main(int argc, char *argv[])
{
  static constexpr auto setup_catalogue = [](){
    return std::to_array<l18n::entry>(
        {
          { "foo", "foo_sv" },
          { "bar", "bar_sv" },
          { "baz", "baz_sv" },
        });
  };

  static constexpr auto setup_catalogue2 = [](){
    return std::to_array<l18n::entry>(
        {
          { "foo", "foo_en" },
          { "bar", "bar_en" },
          { "baz", "baz_en" },
        });
  };

  static constexpr auto setup_catalogues = [](){
    return std::to_array({
        l18n::create_catalogue(setup_catalogue, "sv_SE"),
        l18n::create_catalogue(setup_catalogue2, "en_UK")
    });
  };

  constexpr auto my_library = l18n::create_library(setup_catalogues);

  std::cerr << my_library.gettext<"foo">()(0) << "\n";
  std::cerr << my_library.gettext<"bar">()(argc - 1) << "\n";
  std::cerr << my_library.gettext_dynamic("foo")(0) << "\n";
//  std::cerr << l18n::create_library(setup_catalogues).gettext<"bar">().locale(0) << "\n";
//  std::cerr << l18n::create_library(setup_catalogues).gettext<"baz">().locale(argc - 1) << "\n";

  return 0;
}

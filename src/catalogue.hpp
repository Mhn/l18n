#pragma once

#include <array>
#include <string_view>
#include <type_traits>
#include <fixed_string.hpp>

namespace l18n
{

  struct entry
  {
    std::string_view key;
    std::string_view value;
  };

  template <size_t N>
  struct multi_entry
  {
    std::string_view key;
    std::array<std::string_view, N> value;
  };

  template <size_t NumLocales>
  struct runtime_entry
  {
    std::array<std::string_view, NumLocales> const values;

    constexpr std::string_view operator ()(size_t locale) const
    {
      return values[locale];
    }
  };

  template <size_t NumLocales, size_t NumEntries>
  class library
  {
    public:
      constexpr library(auto entries)
        : m_entries(entries)
      {
      }

      template <fixstr::fixed_string key>
      consteval auto gettext() const
      {
        for (auto const & e : m_entries)
          if (e.key == key)
//            return [v = e.value] (size_t locale) { return v[locale]; };
            return runtime_entry<NumLocales>{e.value};

        throw std::invalid_argument("Key not found in library: " + std::string{key});
      }

      auto gettext_dynamic(std::string_view key) const
      {
        for (auto const & e : m_entries)
          if (e.key == key)
            return [v = e.value] (size_t locale) { return v[locale]; };

        throw std::invalid_argument("Key not found in library: " + std::string{key});
      }

      const std::array<multi_entry<NumLocales>, NumEntries> m_entries;
  };

  template <size_t NumEntries, size_t NumLocales>
  constexpr auto to_library(std::array<multi_entry<NumLocales>, NumEntries> const & catalogues)
  {
    return library<NumLocales, NumEntries>(catalogues);
  }

  template <size_t NumEntries, size_t NumLocales>
  constexpr bool check_valid_catalogues(std::array<std::array<entry, NumEntries>, NumLocales> const & catalogues)
  {
    for (auto e = 0; e < NumEntries; e++)
      for (auto l = 0; l < NumLocales; l++)
        if (catalogues[0][e].key != catalogues[l][e].key)
          return false;
    return true;
  }

  template <size_t NumEntries, size_t NumLocales>
  constexpr auto transform_entries(std::array<std::array<entry, NumEntries>, NumLocales> const & catalogues)
  {
    std::array<multi_entry<NumLocales>, NumEntries> ret;
    for (auto e = 0; e < NumEntries; e++)
    {
      ret[e].key = catalogues[0][e].key;
      for (auto l = 0; l < NumLocales; l++)
        ret[e].value[l] = catalogues[l][e].value;
    }
    return ret;
  }

  constexpr auto create_catalogue(auto create_lambda, std::string_view locale)
  {
    return create_lambda();
  }

  constexpr auto create_library(auto create_lambda)
  {
    constexpr auto catalogues = create_lambda();
    static_assert(check_valid_catalogues(catalogues), "Missmatching keys in catalogue");
    return to_library(transform_entries(catalogues));
  }
}
